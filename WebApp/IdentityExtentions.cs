﻿using DataAccess.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApp
{
    public static class IdentityExtentions
    {
        public static IEnumerable<Claim> GetClaims(this Identity identity)
        {
            return new List<Claim>
            {
                new Claim("Login", identity.Login),
                identity.IdentityType.GetClaim()
            };
        }
    }
}
