﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using DataAccess.Repositories.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class AccountController : Controller
    {
        private IIdentityRepo<Identity> _repo;

        public AccountController(IIdentityRepo<Identity> repo)
        {
            _repo = repo;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(Identity identity)
        {
            if (_repo.CheckIdentity(identity))
            {
                var user = _repo.Identities.FirstOrDefault(idnt => idnt.Login == identity.Login && idnt.Password == identity.Password);
                await Authenticate(user);
                return RedirectToAction("Products", "Home");
            }
            ViewBag.Message = "Пароль и логин не совпадают";
            return View("Index");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            var user = User as ClaimsPrincipal;
            var identity = user.Identity as ClaimsIdentity;
            var claim = identity.Claims.FirstOrDefault(clm => clm.Type == "UserType");
            if(claim != null)
                identity.RemoveClaim(claim);
            return View("Index");
        }

        private async Task Authenticate(Identity user)
        {
            ClaimsIdentity id = new ClaimsIdentity(user.GetClaims(), "ApplicationCookie"
                , ClaimsIdentity.DefaultNameClaimType
                , ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id),
                new AuthenticationProperties { IsPersistent = true, AllowRefresh = true});
        }
    }
}