﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using DataAccess.Repositories.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private IProductRepo<ProductEntityBase> _repo;

        public HomeController(IProductRepo<ProductEntityBase> repo)
        {
            _repo = repo;
        }

        public IActionResult Index()
        {
            if (User.HasClaim("UserType", "User"))
                return View();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [Authorize(Policy = "User")]
        public IActionResult Products()
        {
            return View("Products",_repo.Products);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public async Task<IActionResult> GenerateDbContent()
        {
            try
            {
                var rd = new Random();
                for(int i = 9; i < 20; i++)
                {
                    _repo.AddOrUpdateProduct(new ProductEntityBase
                    {
                        Name = "a" + i.ToString(),
                        Width = rd.Next(0,50),
                        Height = rd.Next(0,10),
                        Depth = rd.Next(15,70)
                    });
                }

                _repo.SaveChanges();
            }
            catch(Exception ex)
            {
                return Error();
            }

            return Ok();
        }
    }
}
