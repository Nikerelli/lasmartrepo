﻿using DataAccess.Interfaces;
using DataAccess.Repositories.Models;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Repositories
{
    public class ProductLasmartRepo: IProductRepo<ProductEntityBase>
    {
        private readonly LasmartContext _ctx;
        
        public ProductLasmartRepo(LasmartContext context)
        {
            _ctx = context;
        }

        public IList<ProductEntityBase> Products
        {
            get
            {
                return _ctx.Products.ToList();
            }
        }

        public void AddOrUpdateProduct(ProductEntityBase product)
        {
            ProductEntityBase dbProduct = _ctx.Products.FirstOrDefault(pr => pr.Id == product.Id);
            if (dbProduct != null && !dbProduct.Equals(product))
            {
                dbProduct = product;
                _ctx.Products.Update(dbProduct);
            }
            else
            {
                _ctx.Products.Add(product);
            }
        }

        public void RemoveProduct(ProductEntityBase product)
        {
            ProductEntityBase dbProduct = _ctx.Products.FirstOrDefault(pr => pr.Id == product.Id);
            if (dbProduct != null)
            {
                dbProduct = product;
                _ctx.Products.Remove(dbProduct);
            }
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }

    }
}
