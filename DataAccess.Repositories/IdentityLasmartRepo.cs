﻿using DataAccess.Interfaces;
using DataAccess.Repositories.Models;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Repositories
{
    public class IdentityLasmartRepo : IIdentityRepo<Identity>
    {
        private readonly LasmartContext _ctx;

        public IdentityLasmartRepo(LasmartContext context)
        {
            _ctx = context;
        }

        public IList<Identity> Identities
        {
            get
            {
                return _ctx.Identities.ToList();
            }
        }

        public bool CheckIdentity(string login, string pswd)
        {
            return _ctx.Identities.FirstOrDefault(idt => idt.Login == login && idt.Password == pswd) != null;
        }

        public bool CheckIdentity(Identity identity)
        {
            return _ctx.Identities.FirstOrDefault(idt => idt.Login == identity.Login && idt.Password == identity.Password) != null;
        }

        public void AddOrUpdateIdentity(Identity identity)
        {
            Identity dbIdentity = _ctx.Identities.FirstOrDefault(pr => pr.Id == identity.Id);
            if (!dbIdentity.Equals(identity))
            {
                dbIdentity = identity;
                _ctx.Identities.Update(dbIdentity);
            }
            else
            {
                _ctx.Identities.Add(identity);
            }
        }

        public void RemoveIdentity(Identity identity)
        {
            Identity dbIdentity = _ctx.Identities.FirstOrDefault(pr => pr.Id == identity.Id);
            if (dbIdentity != null)
            {
                dbIdentity = identity;
                _ctx.Identities.Remove(dbIdentity);
            }
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }
    }
}
