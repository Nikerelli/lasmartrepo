﻿using DataAccess.Repositories.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace DataAccess.Repositories
{
    public class LasmartContext : DbContext
    {
        public virtual DbSet<Identity> Identities { get;set; }
        public virtual DbSet<ProductEntityBase> Products { get; set; }

        public LasmartContext(DbContextOptions builder) :
            base(builder)
        {

        }
    }
}
