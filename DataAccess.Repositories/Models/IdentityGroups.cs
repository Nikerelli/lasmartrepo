﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace DataAccess.Repositories.Models
{
    public enum IdentityGroups
    {
        Unknown,
        User
    }

    public static class GroupsExtentions
    {
        public static Claim GetClaim(this IdentityGroups group)
        {
            switch (group)
            {
                case IdentityGroups.User:
                    return new Claim("UserType", group.ToString());
                default:
                    return new Claim("UserType", group.ToString());
            }
        }
    }

}
