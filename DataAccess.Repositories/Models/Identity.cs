﻿using DataAccess.Interfaces;
using System;

namespace DataAccess.Repositories.Models
{
    public sealed class Identity: EntityBase, IEquatable<Identity>
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public IdentityGroups IdentityType { get; set; }

        public bool Equals(Identity identity)
        {
            return identity.Id == Id && identity.Login == Login && identity.Password == Password;
        }
    }
}
