﻿using DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories.Models
{
    public sealed class ProductEntityBase : EntityBase, IProductBase, IEquatable<ProductEntityBase>
    {
        public string Name { get; set; }

        public float Width { get; set; }

        public float Height { get; set; }

        public float Depth { get; set; }

        public bool Equals(ProductEntityBase product)
        {
            return Id == product.Id && Name == product.Name && Width == product.Width && product.Height == Height && product.Depth == Depth;
        }
    }
}
