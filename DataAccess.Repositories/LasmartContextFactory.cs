﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{
    public class LasmartContextFactory
    {
        private readonly string _connectionString;

        public LasmartContextFactory(string connection)
        {
            _connectionString = connection;
        }

        public LasmartContext CreateDbContext()
        {
            return new LasmartContext(new DbContextOptionsBuilder()
                .UseSqlServer(_connectionString)
                .Options);
        }
    }
}
