﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Interfaces
{
    public interface IProductBase
    {
        string Name { get; set; }

        float Width { get; set; }

        float Height { get; set; }

        float Depth { get; set; }
    }
}
