﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Interfaces
{
    public interface IProductRepo<TModel> : IRepoBase where TModel : EntityBase
    {
        IList<TModel> Products { get; }

        void AddOrUpdateProduct(TModel identity);
        void RemoveProduct(TModel identity);

    }
}
