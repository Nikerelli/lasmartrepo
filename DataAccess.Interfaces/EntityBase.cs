﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Interfaces
{
    public abstract class EntityBase
    {
        public int Id { get; set; }
    }
}
