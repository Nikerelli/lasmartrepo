﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Interfaces
{
    public interface IIdentityRepo<TModel> : IRepoBase where TModel : EntityBase
    {
        IList<TModel> Identities { get; }

        void AddOrUpdateIdentity(TModel identity);
        void RemoveIdentity(TModel identity);
        bool CheckIdentity(TModel identity);
        bool CheckIdentity(string login,string pswd);
    }
}
