﻿using DataAccess.Interfaces;
using DataAccess.Repositories;
using DataAccess.Repositories.Models;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ServiceRegistration
{
    public static class ServiceRegistration
    {
        public static void AddLasmartContextFactory(this IServiceCollection services,string conString)
        {
            var contextFact = new LasmartContextFactory(conString);
            services.AddSingleton(provider => contextFact);
        }

        public static void AddProductLasmartRepo(this IServiceCollection services)
        {
            services.AddTransient<IProductRepo<ProductEntityBase>>(provider => new ProductLasmartRepo(provider.GetService<LasmartContextFactory>().CreateDbContext()));
            services.AddTransient<IIdentityRepo<Identity>>(provider => new IdentityLasmartRepo(provider.GetService<LasmartContextFactory>().CreateDbContext()));
        }
    }
}
